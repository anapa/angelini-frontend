<?php include "inc/header.php" ?>

    <div class="container-fluid welcome_background">
        <div class="row form_row">
            <div class="col-md-6 col-lg-6 col-xl-6 col-6  mt-2">
                <h4>Welcome , Name Surname</h4>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-6 col-6  mt-2">
                <a href="" class="btn btn-primary logout_btn">Logout</a>
            </div>
        </div>


        <div class="row">
            <div class="col-12 col-md-8 col-lg-8 col-xl-8 mx-auto mt-5">
                <h1>Contramid Day</h1>
                <h2>October 10th, Barcelona</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrum
                    exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis
                    aute iure reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.</p>
                <form id="welcome_form" action="" method="post">
                    <div class="form-group row">
                        <div class="col-md-6 col-12">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">First name</div>
                                </div>
                                <input type="text" class="form-control" id="first_name" name="first_name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 col-12">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Second name</div>
                                </div>
                                <input type="text" class="form-control" id="second_name" name="second_name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 col-12">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Email</div>
                                </div>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                        </div>
                    </div>
                    <a href="#" class="mt-3">Change password</a>

                    <div class="form-group mt-3 row">
                        <div class="col-md-6 col-12">
                            <label for="inputlg">Team</label>
                            <input class="form-control input-lg" id="team_value" name="team_value"
                                   value="ValueFromDatabase" type="text">
                        </div>
                    </div>
                    <h1>This is title</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrum
                        exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.
                        Quis
                        aute iure reprehenderit in voluptate velit.</p>
                    <div class="row py-5 mb-5">
                        <div class="col-12 col-md-8 col-lg-8 col-xl-8 mx-auto text-center">
                            <a href="" class="btn btn-outline-primary btn-lg welcome_lg_btn">
                                <img src="assets/images/baseline-done_outline-24px.svg" class="img-fluid mr-3">Question Time</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include "inc/footer.php" ?>