<?php include "inc/header.php" ?>

    <div class="container-fluid welcome_background">
        <div class="row form_row">
            <div class="col-md-6 col-lg-6 col-xl-6 col-6  mt-2">
                <h4>Welcome , Name Surname</h4>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-6 col-6  mt-2">
                <a href="" class="btn btn-primary logout_btn">Logout</a>
            </div>
        </div>


        <div class="row">
            <div class="col-12 col-md-8 col-lg-8 col-xl-8 mx-auto mt-5 thank_you text-center">
                <h1><img src="assets/images/baseline-done_outline-24px.svg" class="img-fluid mr-3">Thank you</h1>


            </div>
        </div>
    </div>

<?php include "inc/footer.php" ?>