<?php include "inc/header.php"?>

<div class="container-fluid">
    <div class="row form_row">
        <div class="col-md-8 col-lg-8 col-xl-8 col-12 mx-auto mt-2">
            <form id="login_form" method="post" action="#">
                <div class="form-row ">
                    <div class="col-auto">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><img src="assets/images/baseline-person-24px.svg"
                                                                   class="img-fluid"></div>
                            </div>
                            <input type="text" class="form-control" id="usename" name="username" placeholder="Username">
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><img src="assets/images/baseline-lock-24px.svg"
                                                                   class="img-fluid"></div>
                            </div>
                            <input type="text" class="form-control" id="password" name="password"
                                   placeholder="password">
                        </div>
                    </div>

                    <div class="col-auto">
                        <button type="submit" class="btn btn-primary mb-2">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row row_slider ">
        <div class="col-md-12 col-12 col-lg-12 col-xl-12 p-0">
            <img src="assets/images/main_img_1920x1280.jpg" class="img-fluid">
        </div>
    </div>
</div>

<?php include "inc/footer.php"?>