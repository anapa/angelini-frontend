<div class="container-fluid footer">
    <div class="row mt-3">
        <div class="col-md-6 col-lg-6 col-xl-6 col-6 text-center ">
            <img src="../assets/images/Angelini_logo.png" class="img-fluid">
        </div>
        <div class="col-md-4 col-lg-4 col-xl-4 col-6 mx-auto">
            <h1>Towards a new goal.</h1>
            <h2><strong>Together.</strong></h2>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>