<?php include "inc/header.php" ?>

    <div class="container-fluid welcome_background">
        <div class="row form_row">
            <div class="col-md-6 col-lg-6 col-xl-6 col-6  mt-2">
                <h4>Welcome , Name Surname</h4>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-6 col-6  mt-2">
                <a href="" class="btn btn-primary logout_btn">Logout</a>
            </div>
        </div>


        <div class="row">
            <div class="col-12 col-md-8 col-lg-8 col-xl-8 mx-auto mt-5 questions_form">
                <h1><img src="assets/images/baseline-done_outline-24px.svg" class="img-fluid mr-3">Question #01</h1>

                <h3 class="mt-3">Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur?</h3>
                <form id="question_form" action="" method="post" class="mt-5">
                    <div class="form-check">
                        <input class="form-check-input question_checkbox" type="checkbox" value="" id="defaultCheck1" name="defaultCheck1">
                        <label class="form-check-label margin_left50" for="defaultCheck1">
                            Lorem ipsum dolor sit amet
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input question_checkbox" type="checkbox" value="" id="defaultCheck2" name="defaultCheck2">
                        <label class="form-check-label margin_left50" for="defaultCheck2">
                            Lorem ipsum dolor sit amet
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input question_checkbox" type="checkbox" value="" id="defaultCheck3" name="defaultCheck3">
                        <label class="form-check-label margin_left50" for="defaultCheck3">
                            Lorem ipsum dolor sit amet
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input question_checkbox" type="checkbox" value="" id="defaultCheck3" name="defaultCheck3">
                        <label class="form-check-label margin_left50" for="defaultCheck3">
                            Lorem ipsum dolor sit amet
                        </label>
                    </div>

                    <div class="row py-5 mb-5 mt-5">
                        <div class="col-12 col-md-8 col-lg-8 col-xl-8 mx-auto text-center">
                            <input type="submit" class="btn btn-primary btn-lg question_ok_button" value="OK">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include "inc/footer.php" ?>